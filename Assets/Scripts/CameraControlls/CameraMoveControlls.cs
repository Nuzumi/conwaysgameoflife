using Unity.Mathematics;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Assets.Scripts.CameraControlls
{
    public class CameraMoveControlls : MonoBehaviour
    {
        [SerializeField] private Camera mainCamera;
        [SerializeField] private float cameraSpeed;
        [SerializeField] private float zoomSpeed;
        [SerializeField] private float minSize;

        private Inputs.CameraActions cameraActions;

        private void Awake()
        {
            cameraActions = new Inputs().camera;
            cameraActions.Enable();

            cameraActions.Zoom.performed += OnCameraZoom;
        }

        private void OnDestroy()
        {
            cameraActions.Zoom.performed -= OnCameraZoom;
        }

        private void Update()
        {
            var movement = cameraActions.Movement.ReadValue<Vector2>();
            var moveVector2d = movement * (Time.deltaTime * cameraSpeed * mainCamera.orthographicSize);
            transform.position += new Vector3(moveVector2d.x, 0, moveVector2d.y);
        }

        private void OnCameraZoom(InputAction.CallbackContext context)
        {
            var zoom = context.ReadValue<Vector2>().y * zoomSpeed * Time.deltaTime;
            mainCamera.orthographicSize += zoom;

            mainCamera.orthographicSize = math.max(minSize, mainCamera.orthographicSize);
        }
    }
}