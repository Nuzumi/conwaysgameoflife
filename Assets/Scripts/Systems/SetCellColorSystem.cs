﻿using Assets.Scripts.Authorings;
using Unity.Burst;
using Unity.Entities;
using Unity.Rendering;

namespace Assets.Scripts.Systems
{

    [DisableAutoCreation]
    public partial struct SetCellColorSystem : ISystem
    {
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<Cell>();
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            new SetCellColorJob
            {
                colorConfig = SystemAPI.GetSingleton<ColorConfig>()
            }.ScheduleParallel();
        }
    }

    [BurstCompile]
    public partial struct SetCellColorJob : IJobEntity
    {
        public ColorConfig colorConfig;

        public void Execute(ref URPMaterialPropertyBaseColor baseColor, in Cell cell)
        {
            baseColor.Value = cell.isOn == 1 ? colorConfig.enabledColor : colorConfig.disabledColor;
        }
    }
}