using Assets.Scripts.Authorings;
using System;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Assets.Scripts.Systems
{

    public partial struct SpawnCellsSystem : ISystem
    {
        private static readonly int[] indexOffsets = { -1, 0, 1 };

        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<SpawnConfig>();
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var spawnConfigRW = SystemAPI.GetSingletonRW<SpawnConfig>();
            if (!spawnConfigRW.ValueRO.shouldSpawn)
            {
                return;
            }

            spawnConfigRW.ValueRW.shouldSpawn = false;
            var spawnConfig = spawnConfigRW.ValueRO;

            var rotation = quaternion.RotateX(math.radians(90));
            var spawned = state.EntityManager.Instantiate(spawnConfig.cellEntityPrefab, spawnConfig.cellsAmount, state.WorldUpdateAllocator);

            for(int i = 0;  i < spawnConfig.cellsAmount; i++)
            {
                var entity = spawned[i];
                SystemAPI.SetComponent(entity, new Cell
                {
                    index = i,
                });
                SystemAPI.SetComponent(entity, new LocalTransform
                {
                    Position = GetPosition(i, spawnConfig.cellsGridSize),
                    Rotation = rotation,
                    Scale = 1
                });
                
                SetNeighbours(SystemAPI.GetBuffer<CellNeighbourIndex>(entity), i, spawnConfig.cellsGridSize);
            }
        }

        private float3 GetPosition(int index, int cellGridSize)
        {
            var x = index % cellGridSize;
            var z = index / cellGridSize;
            return new float3(x, 0, z);
        }

        private void SetNeighbours(DynamicBuffer<CellNeighbourIndex> neighbourBuffer, int index, int cellGridSize)
        {
            var column = index % cellGridSize;
            var row = index / cellGridSize;

            var tmpIndexes = new NativeArray<int>(8, Allocator.Temp);
            var i = 0;

            foreach (var rowOffset in indexOffsets)
            {
                foreach(var columnOffset in indexOffsets)
                {
                    if (rowOffset == 0 && columnOffset == 0)
                    {
                        continue;
                    }

                    int neighborRow = (row + rowOffset + cellGridSize) % cellGridSize;
                    int neighborCol = (column + columnOffset + cellGridSize) % cellGridSize;

                    int neighborIndex = neighborRow * cellGridSize + neighborCol;

                    tmpIndexes[i++] = neighborIndex;
                }
            }

            tmpIndexes.Sort();

            foreach(var neighbourIndex in tmpIndexes)
            {
                neighbourBuffer.Add(new CellNeighbourIndex
                {
                    index = neighbourIndex,
                });
            }

            tmpIndexes.Dispose();
        }
    }
}