﻿using Assets.Scripts.Authorings;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;

namespace Assets.Scripts.Systems
{

    public partial struct CellSimulationSystem : ISystem
    {
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<SimulationConfig>();
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            if (!SystemAPI.GetSingleton<SimulationConfig>().enabled)
            {
                return;
            }

            var config = SystemAPI.GetSingleton<SpawnConfig>();
            var stateArray = CollectionHelper.CreateNativeArray<byte>(config.cellsAmount, state.WorldUpdateAllocator);

            var currentStateJob = new CurrentStateJob
            {
                stateArray = stateArray,
            }.ScheduleParallel(state.Dependency);

            currentStateJob.Complete();

            var simulationJob = new SetCellsState
            {
                stateArray = stateArray.AsReadOnly(),
                gridSize = config.cellsGridSize
            }.ScheduleParallel(state.Dependency);

            simulationJob.Complete();
        }
    }

    [BurstCompile]
    public partial struct CurrentStateJob : IJobEntity
    {
        [NativeDisableParallelForRestriction]
        [WriteOnly]
        public NativeArray<byte> stateArray;

        public void Execute(in Cell cell)
        {
            stateArray[cell.index] = cell.isOn;
        }
    }

    [BurstCompile]
    public partial struct SetCellsState : IJobEntity
    {
        [NativeDisableParallelForRestriction]
        [ReadOnly] public NativeArray<byte>.ReadOnly stateArray;
        public int gridSize;

        public void Execute(ref Cell cell, in DynamicBuffer<CellNeighbourIndex> neighbours)
        {
            var activeNeighbours = GetActiveNeighboursCount(neighbours);

            cell.isOn = cell.isOn != 0
                ? GetActiveCellNextState(activeNeighbours)
                : GetNotActiveNextState(activeNeighbours);
        }

        private int GetActiveNeighboursCount(in DynamicBuffer<CellNeighbourIndex> neighbours)
        {
            var activeCount = 0;

            for (int i = 0; i < neighbours.Length; i++)
            {
                var index = neighbours[i].index;
                activeCount += stateArray[index];
            }

            return activeCount;
        }

        private static byte GetActiveCellNextState(in int activeNeighbours) => activeNeighbours switch
        {
            < 2 => 0,
            < 4 => 1,
            _ => 0
        };

        private static byte GetNotActiveNextState(in int activeNeighbours) => activeNeighbours switch
        {
            3 => 1,
            _ => 0
        };
    }
}