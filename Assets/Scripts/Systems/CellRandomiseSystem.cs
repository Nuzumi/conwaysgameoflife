﻿using Assets.Scripts.Authorings;
using Unity.Burst;
using Unity.Entities;
using Unity.Mathematics;

namespace Assets.Scripts.Systems
{
    public partial struct CellRandomiseSystem : ISystem
    {
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<Cell>();
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var config = SystemAPI.GetSingletonRW<RandomiseConfig>();

            if (!config.ValueRO.shouldRandomise)
            {
                return;
            }

            config.ValueRW.shouldRandomise = false;
            var random = Random.CreateFromIndex((uint)SystemAPI.Time.ElapsedTime);

            foreach(var cell in SystemAPI.Query<RefRW<Cell>>())
            {
                var randomValue = random.NextFloat();
                cell.ValueRW.isOn = randomValue <= config.ValueRO.value ? (byte)1 : (byte)0;
            }
        }
    }
}