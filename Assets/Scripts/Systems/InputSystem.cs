﻿using Assets.Scripts.Authorings;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace Assets.Scripts.Systems
{
    public partial class InputSystem : SystemBase
    {
        private Camera camera;

        private Inputs.CellsActions cellsActions;

        private int gridSize;
        private float2 lastMousePosition;
        private bool shouldSetCell;
        private float positionScale;

        protected override void OnCreate()
        {
            cellsActions = new Inputs().cells;
            RequireForUpdate<Cell>();

            cellsActions.Set.performed += OnSetPerformed;
            cellsActions.Set.canceled += OnSetCanceled;
        }

        protected override void OnDestroy()
        {
            cellsActions.Set.performed -= OnSetPerformed;
            cellsActions.Set.canceled -= OnSetCanceled;
        }

        protected override void OnStartRunning()
        {
            cellsActions.Enable();
            camera = Camera.main;
            gridSize = SystemAPI.GetSingleton<SpawnConfig>().cellsGridSize;
            positionScale = gridSize / 100f;
        }

        protected override void OnUpdate()
        {
            if (!shouldSetCell)
            {
                return;
            }

            var currentPosition = GetMouseWorldPosition();

            if (math.all(currentPosition == lastMousePosition))
            {
                return;
            }

            lastMousePosition = currentPosition;

            var scaledPosition = currentPosition * positionScale;
            var scaledIntPosition = new int2((int)scaledPosition.x, (int)scaledPosition.y);

            var cellIndex = scaledIntPosition.y * gridSize + scaledIntPosition.x;

            foreach(var cell in SystemAPI.Query<RefRW<Cell>>())
            {
                if(cell.ValueRO.index == cellIndex)
                {
                    cell.ValueRW.isOn = cell.ValueRO.isOn == 0 ? (byte)1 : (byte)0;
                    return;
                }
            }
            
        }

        private float2 GetMouseWorldPosition()
        {
            var mousePosition = cellsActions.Position.ReadValue<Vector2>();
            var worldPosition = (float3)camera.ScreenToWorldPoint(mousePosition);
            return worldPosition.xz;
        }

        private void OnSetCanceled(InputAction.CallbackContext context) => shouldSetCell = false;

        private void OnSetPerformed(InputAction.CallbackContext context)
        {
            shouldSetCell = true;
        }
    }
}