﻿using Assets.Scripts.Authorings;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

namespace Assets.Scripts.Systems
{
    [UpdateAfter(typeof(CellSimulationSystem))]
    public partial class SetTextureDataSystem : SystemBase
    {
        protected override void OnCreate()
        {
            RequireForUpdate<Cell>();
        }

        protected override void OnUpdate()
        {
            var query = SystemAPI.QueryBuilder().WithAll<DisplayConfig>().Build();
            var texture = query.GetSingleton<DisplayConfig>().texture;

            var data = texture.GetPixelData<Color32>(0);

            var job = new SetTextureDataJob
            {
                colorConfig = SystemAPI.GetSingleton<ColorConfig>(),
                data = data
            }.ScheduleParallel(Dependency);

            job.Complete();
            texture.Apply();
        }
    }

    [BurstCompile]
    public partial struct SetTextureDataJob : IJobEntity
    {
        [NativeDisableParallelForRestriction] 
        public NativeArray<Color32> data;
        public ColorConfig colorConfig;

        public void Execute(in Cell cell)
        {
            data[cell.index] = cell.isOn == 1 ? colorConfig.enabledColor32 : colorConfig.disabledColor32; 
        }
    }
}