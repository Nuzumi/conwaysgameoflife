using Assets.Scripts.Authorings;
using TMPro;
using Unity.Entities;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Ui
{

    public class MenuPanel : MonoBehaviour
    {
        [SerializeField] private GameObject panel;
        [SerializeField] private Button startButton;
        [SerializeField] private TMP_InputField sizeInputField;
        [SerializeField] private TextureController textureController;

        private EntityManager entityManager;
        private EntityQuery query;

        private void Awake()
        {
            startButton.onClick.AddListener(StartGameOfLife);
            entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            query = entityManager.CreateEntityQuery(typeof(SpawnConfig));
        }

        private void OnDestroy()
        {
            startButton.onClick.RemoveAllListeners();
        }

        private void StartGameOfLife()
        {
            if(!int.TryParse(sizeInputField.text, out var size))
            {
                size = 256;
            }

            textureController.CreateTexture(size);
            var config = query.GetSingletonRW<SpawnConfig>();
            config.ValueRW.cellsGridSize = size;
            config.ValueRW.cellsAmount = size * size;
            config.ValueRW.shouldSpawn = true;
            panel.SetActive(false);
        }
    }
}