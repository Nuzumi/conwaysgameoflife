using Assets.Scripts.Authorings;
using TMPro;
using Unity.Entities;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.Ui
{
    public class ControllPanel : MonoBehaviour
    {
        [SerializeField] private Button startStopButton;
        [SerializeField] private Button restartButton;
        [SerializeField] private Button randomiseButton;
        [SerializeField] private TextMeshProUGUI startStopButtonText;

        private EntityManager entityManager;
        private EntityQuery simulationConfigQuery;
        private EntityQuery randomiseQuery;

        private void Awake()
        {
            entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            simulationConfigQuery = entityManager.CreateEntityQuery(typeof(SimulationConfig));
            randomiseQuery = entityManager.CreateEntityQuery(typeof(RandomiseConfig));
            startStopButton.onClick.AddListener(ChangeSimulationState);
            restartButton.onClick.AddListener(Restart);
            randomiseButton.onClick.AddListener(RandomiseCells);
        }

        private void OnDestroy()
        {
            startStopButton.onClick.RemoveAllListeners();
            restartButton.onClick.RemoveAllListeners();
            randomiseButton.onClick.AddListener(RandomiseCells);
        }

        private void ChangeSimulationState()
        {
            var simulationConfig = simulationConfigQuery.GetSingletonRW<SimulationConfig>();
            simulationConfig.ValueRW.enabled = !simulationConfig.ValueRO.enabled;

            startStopButtonText.text = simulationConfig.ValueRO.enabled ? "stop" : "start";
        }

        private void Restart()
        {
            SceneManager.LoadScene(0, LoadSceneMode.Single);
        }

        private void RandomiseCells()
        {
            var config = randomiseQuery.GetSingletonRW<RandomiseConfig>();
            config.ValueRW.shouldRandomise = true;
        }
    }
}