﻿using Assets.Scripts.Authorings;
using Unity.Entities;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Ui
{
    public class TextureController : MonoBehaviour
    {
        [SerializeField] private RectTransform rectTransform;
        [SerializeField] private RawImage rawImage;

        private Texture2D texture;

        public void CreateTexture(int size)
        {
            texture = new Texture2D(size, size, TextureFormat.RGBA32 , false, true);
            rawImage.texture = texture;

            var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            var query = entityManager.CreateEntityQuery(typeof(DisplayConfig));
            var config = query.GetSingletonRW<DisplayConfig>();
            config.texture = texture;
        }
    }
}