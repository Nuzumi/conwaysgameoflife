﻿using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Assets.Scripts.Authorings
{

    public class ConfigAuthoring : MonoBehaviour
    {
        public GameObject cellPrefab;
        public int cellsGridSize;

        public Color enabledColor;
        public Color disabledColor;

        public class Baker : Baker<ConfigAuthoring>
        {
            public override void Bake(ConfigAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.None);
                AddComponent<Config>(entity);
                AddComponent(entity, new SimulationConfig
                {
                    enabled = false
                });
                AddComponent(entity, new SpawnConfig
                {
                    cellsGridSize = authoring.cellsGridSize,
                    cellEntityPrefab = GetEntity(authoring.cellPrefab, TransformUsageFlags.Renderable),
                    cellsAmount = authoring.cellsGridSize * authoring.cellsGridSize,
                    shouldSpawn = false
                });
                AddComponent(entity, new ColorConfig
                {
                    disabledColor = ToFloat(authoring.disabledColor),
                    enabledColor = ToFloat(authoring.enabledColor),
                    enabledColor32 = authoring.enabledColor,
                    disabledColor32 = authoring.disabledColor,
                });
                AddComponent(entity, new RandomiseConfig
                {
                    shouldRandomise = true,
                    value = .4f
                });
            }

            private float4 ToFloat(Color color)
            {
                return new float4(
                    color.linear.r,
                    color.linear.g,
                    color.linear.b,
                    color.linear.a);
            }
        }
    }

    public struct Config : IComponentData
    {

    }

    public struct SpawnConfig : IComponentData
    {
        public Entity cellEntityPrefab;
        public int cellsGridSize;
        public int cellsAmount;
        public bool shouldSpawn;
    }

    public struct ColorConfig : IComponentData
    {
        public float4 enabledColor;
        public Color32 enabledColor32;
        public float4 disabledColor;
        public Color32 disabledColor32;
    }

    public struct SimulationConfig : IComponentData
    {
        public bool enabled;
    }

    public struct RandomiseConfig : IComponentData
    {
        public float value;
        public bool shouldRandomise;
    }

    public class DisplayConfig : IComponentData
    {
        public Texture2D texture;
    }
}