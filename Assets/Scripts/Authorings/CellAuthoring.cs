using Unity.Entities;
using UnityEngine;

namespace Assets.Scripts.Authorings
{

    public class CellAuthoring : MonoBehaviour
    {
        public class Baker : Baker<CellAuthoring>
        {
            public override void Bake(CellAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.None);
                AddComponent<Cell>(entity);
                AddBuffer<CellNeighbourIndex>(entity);
            }
        }
    }

    public struct Cell : IComponentData
    {
        public int index;
        public byte isOn;
    }

    [InternalBufferCapacity(8)]
    public struct CellNeighbourIndex : IBufferElementData
    {
        public int index;
    }
}