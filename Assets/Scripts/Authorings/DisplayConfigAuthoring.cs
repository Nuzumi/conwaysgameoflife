﻿using Unity.Entities;
using UnityEngine;

namespace Assets.Scripts.Authorings
{
    public class DisplayConfigAuthoring : MonoBehaviour
    {
        public class Baker : Baker<DisplayConfigAuthoring>
        {
            public override void Bake(DisplayConfigAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.None);
                AddComponentObject(entity, new DisplayConfig());
            }
        }
    }
}